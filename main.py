import random


def save_occ_file(occ: dict) -> None:
    """
    Saves the occurences of words
    :param occ: The dict of occurences
    :return: Nothing
    """
    with open('occ.txt', 'w', encoding='utf-8') as occ_file:
        # Sorts the occurences by number of occurences in reverse order
        for word in sorted([[key, occ[key]] for key in occ], key=lambda x: x[1], reverse=True):
            occ_file.write(str(word) + '\n')


def create_dict(path: str, save_occ: bool = False) -> dict:
    """
    Parses a file and returns a dict
    :param path: the file to open
    :param save_occ: Whether you want to save the occurences
    :return: a dict {word : [next_words]}
    """
    with open(path, 'r', encoding='utf-8') as file:
        words = file.read().strip(',').lower().split()
    dictio, occ, last = {}, {}, words.pop(0)
    # For each word, add next word to the dict
    for word in words:
        dictio.setdefault(last, []).append(word)
        occ[last] = occ.get(last, 0) + 1
        last = word
    if save_occ:
        save_occ_file(occ)
    return dictio


def markovify(num_words: int, dictio: dict) -> str:
    """
    Use a markov chain to generate a long string
    :param dictio: the dict generated before
    :return: A long string
    """
    current, outstr = random.choice(list(dictio.keys())), ''
    for _ in range(num_words):
        outstr += current + ' '
        current = random.choice(dictio.get(current, random.choice(list(dictio.keys()))))
    return outstr


with open('out.txt', 'w', encoding='utf-8') as out:
    out.write(markovify(1000, create_dict('pwemes.txt', True)))
